ics-ans-nas
===================

Ansible playbook to install nas with nfs share and zfs filesystem.

Uses ics-ans-role-zfs and ics-ans-role-nfs-server

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

License
-------

BSD 2-clause

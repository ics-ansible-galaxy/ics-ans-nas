import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_nfs_service(host):
    nfs = host.service("nfs-server")
    assert nfs.is_enabled
    assert nfs.is_running
